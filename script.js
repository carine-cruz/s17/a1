//initialize variable
//let studentArr = [`John`, `Jane`, `Joe`, `Alex`, `Sam`, `Samuel`, `Cynthia`];
let studentArr = [];

//--------functions ----------

function addStudent(name){
	studentArr.push(name);
	return `${name} is added to the student's list.`
}

function countStudent(){
	return `There are a total of ${studentArr.length} students enrolled.`;
}
	
function printStudents(){
	//sort students alphabetically
	studentArr.sort();

	//print student names individually
	studentArr.forEach(
		function(name){
			console.log(name);
		}
	)
}

function findStudent(keyword){
	//returns new array of filtered students
	//loops through individual elements in array
	let filteredArr =  studentArr.filter(
							function(student){
								return student.toLowerCase().indexOf(keyword.toLowerCase()) !== -1;
							}
					);
	
	if (filteredArr.length == 1){
		return `${filteredArr.toString()} is an enrollee.`;
	} else if (filteredArr.length >= 2){
		return `${filteredArr.join(", ")} are enrollees.`;
	} else {
		return ` ${keyword} is not an enrollee.`;
	}
 
}

function addSection(section){
	return studentArr.map(
			function(student){
				return student += ` - section ${section}`;
			}
	)
}

function removeStudent(name){
	let modifiedName = name[0].toUpperCase() + name.slice(1);
	let iName = studentArr.indexOf(modifiedName);
	let deletedStudent = studentArr.splice(0,1);
	// console.log(modifiedName);
	// console.log(iName);

	return `${deletedStudent} was removed from the student's list.`
}

//printStudents();
// console.log(studentArr);
// console.log(addStudent(`John`));
// console.log(studentArr);
// console.log(addStudent(`Jane`));
// console.log(studentArr);
// console.log(addStudent(`Joe`));
// console.log(studentArr);
// console.log(countStudent());
// printStudents();
//findStudent('joe');
//findStudent('bill');
//findStudent('j');
//addSection('A');
//removeStudent('Joe');
//printStudents();


